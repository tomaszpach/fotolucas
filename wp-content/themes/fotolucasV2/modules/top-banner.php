<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 16.12.2016
 * Time: 22:26
 */
// Grab the metadata from the database
$bgImage = get_post_meta( get_the_ID(), '_top_banner_image', true );
if ($bgImage) {
    $bgImageUrl = 'style="background-image: url(' . $bgImage . ')"';
}
?>


<div id="top-banner" class="module" <?= $bgImage ? $bgImageUrl : null ?>>
	<div class="filter"></div>
	<div class="content" data-aos="zoom-in">
		<div class="logo"></div>
		<div class="by"><h3 class="white">Łukasz Kogut</h3></div>
		<div class="what"><span><h5 class="white">Fotografia Ślubna</h5></span></div>
	</div>
</div>
