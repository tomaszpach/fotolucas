<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 17.12.2016
 * Time: 14:53
 */
$bgImage = get_post_meta( get_the_ID(), '_quote_image', true );
if ($bgImage) {
    $bgImageUrl = 'style="background-image: url(' . $bgImage . ')"';
}
?>

<div id="quotation" class="module" <?= $bgImage ? $bgImageUrl : null ?>>
	<div class="filter"></div>
	<div class="quote col-xs-12">
        <?= wpautop( get_post_meta( get_the_ID(), '_quote_editor', true ) ); ?>
    </div>
</div>
