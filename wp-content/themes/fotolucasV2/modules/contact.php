<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 16.12.2016
 * Time: 23:35
 */ ?>


<div id="contact" class="module">
    <div class="part text col-xs-12 col-sm-6" data-aos="fade-right" data-aos-delay="300">
        <div class="wrapper">
            <h4>Porozmawiajmy</h4>
            <div class="separator short">
                <span></span>
            </div>
            <div class="description">
                <p>Jeśli spodobały Ci się moje pracę, zapraszam do współpracy. Skontaktuj się ze mną i zapytaj o
                    wolny termin - to nic nie kosztuje.</p>
                <p>Wypełnij formularz kontaktowy, który znajduję się poniżej lub zadzwoń, bądź napisz maila.
                    Skontaktuje sie z Tobą najszybciej jak to będzie możliwe.</p>
                <p>Tel: <a href="tel:48515983539">+48 515 983 539</a></p>
                <p><a href="mailto:kontakt@fotolucas.net">kontakt@fotolucas.net</a></p>

            </div>
            <div class="contact-form">
				<?= do_shortcode( '[contact-form-7 id="371" title="Contact home page"]' ) ?>
            </div>

            <div class="clearfix"></div>
        </div>

    </div>

    <div class="part image col-xs-12 col-sm-6" data-aos="fade-left"></div>
    <div class="clearfix"></div>
</div>