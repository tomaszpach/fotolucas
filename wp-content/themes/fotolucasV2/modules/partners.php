<?php
/**
 * Created by PhpStorm.
 * User: NurtureAgency
 * Date: 07.03.2017
 * Time: 21:03
 */ ?>

<div id="partners">
    <div class="row">
        <div class="col-12">
            <h3 class="text-center">Partnerzy:</h3>
        </div>
        <a class="image col-12 col-sm-6" href="http://www.jaksastudio.pl/" target="_blank">
            <img src="http://fotolucas.net/wp-content/uploads/2017/03/jaksa-studio-video-foto-wideofilmowanie-slubow-ptaszkowa.jpg" alt="Jaksa studio video foto wideofilmowanie slubow ptaszkowa">
        </a>
        <div class="text col-12 col-sm-6">Priorytetem naszej pracy jest profesjonalna produkcja nowoczesnych filmów Ślubnych. Aktywnie
            działamy również w tworzeniu filmów reklamowych, teledysków, dzieł niezależnych, animacji graficznych,
            fotografii ślubnej, portretowej, reklamowej oraz sesji fotograficznych. Poświęć chwilę czasu aby zapoznać się z
            efektami naszej pracy, zapraszamy!
        </div>
    </div>
</div>