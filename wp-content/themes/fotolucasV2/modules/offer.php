<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 15.02.2017
 * Time: 20:11
 */ ?>


<div id="offer" class="offer container-fluid">
    <div class="row">
        <div class="col-12">
            <h3 class="text-center">Oferta:</h3>
        </div>
        <div class="text col-12 col-sm-6">
            <h4>PAKIET PODSTAWOWY:</h4>

	        <?php $delay = 0; $step = 300; ?>
            <ul id="anchor-1">
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-1">błogosławieństwo</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-1">uroczystość w kościele</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-1">40 zdjęć w formacie 13x18 do
                    15x21 wklejonych do albumu lub
                    fotoksiążka
                </li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-1">100 zdjęć zapisanych w
                    najwyższej jakości na płycie DVD z indywidualnym nadrukiem lub
                    pendrive
                </li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-1">ekskluzywne etui na płytę DVD
                    (z Waszym zdjęciem) lub pendrive
                </li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-1">prywatna galeria internetowa 6
                    m-cy.
                </li>
            </ul>
        </div>
        <img src="http://fotolucas.net/wp-content/uploads/2016/09/DSC_4022.jpg"
             class="image col-12 col-sm-6 px-0"/>
    </div>

    <div class="row">
        <img src="http://fotolucas.net/wp-content/uploads/2016/01/PiotrekWiola-18.jpg"
             class="image col-12 col-sm-6 px-0"/>
        <div class="text col-12 col-sm-6">
            <h4>PAKIET STANDARD:</h4>

            <?php $delay = 0; $step = 300; ?>
            <ul id="anchor-2">
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-2">błogosławieństwo</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-2">uroczystość w kościele</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>">przyjęcie weselne do pierwszego tańca (włącznie)</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-2">sesja plenerowa w dniu ślubu
                </li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-2">60 - 80 zdjęć w formacie 10x15
                    do 15x21 wklejonych do albumu lub fotoksiążka
                </li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-2">150 - 250 zdjęć zapisanych w
                    najwyższej jakości na płycie DVD z indywidualnym nadrukiem lub pendrive
                </li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-2">ekskluzywne etui na płytę DVD
                    (z Waszym zdjęciem) lub pendrive
                </li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-2">prywatna galeria internetowa 6
                    m-cy.
                </li>
            </ul>
        </div>
    </div>



    <div class="row">
        <div class="text col-12 col-sm-6">
            <h4>PAKIET STANDARD PLUS:</h4>

	        <?php $delay = 0; $step = 300; ?>
            <ul id="anchor-3">
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-3">przygotowania (fryzjer, makijaż, dom Pani Młodej)</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-3">błogosławieństwo,</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-3">uroczystość w kościele</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-3">przyjęcie weselne do oczepin (włącznie)</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-3">sesja plenerowa w dniu ślubu,</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-3">80 - 100 zdjęć w formacie od 10x15 do 15x21
                    wklejonych do albumu lub fotoksiążka</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-3">500 - 700 zdjęć zapisanych w najwyższej jakości
                    na płytach DVD z indywidualnym nadrukiem lub pendrive</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-3">ekskluzywne Etui na płyty DVD (z Waszym zdjęciem)
                    lub pendrive
                </li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-3">prywatna galeria internetowa 6 m-cy</li>
            </ul>
        </div>
        <img src="http://fotolucas.net/wp-content/uploads/2016/10/Fb-3761.jpg"
             class="image col-12 col-sm-6 px-0"/>
    </div>





    <div class="row">
        <img src="http://fotolucas.net/wp-content/uploads/2016/10/Fb-3108.jpg"
             class="image col-12 col-sm-6 px-0"/>
        <div class="text col-12 col-sm-6">
            <h4>PAKIET EXCLUSIVE:</h4>

	        <?php $delay = 0; $step = 300; ?>
            <ul id="anchor-4">
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">sesja narzeczeńska</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">przygotowania (fryzjer, makijaż, dom Pani Młodej)</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">błogosławieństwo,</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">uroczystość w kościele,</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">przyjęcie weselne do oczepin (włącznie)</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">sesja plenerowa w innym dniu niż dzień ślubu</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">100 - 150 zdjęć w formacie od 10x15 do 15x21 wklejonych do albumu lub fotoksiążka</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">700 - 900 zdjęć zapisanych w najwyższej jakości na płytach DVD z indywidualnym nadrukiem lub pendrive</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">ekskluzywne Etui na płyty DVD (z Waszym zdjęciem) lub pendrive.</li>
                <li data-aos="zoom-in" data-aos-delay="<?= $delay; $delay = $delay + $step; ?>" data-aos-anchor="#anchor-4">prywatna galeria internetowa 6 m-cy</li>
            </ul>
        </div>
    </div>



    <div class="row">
        <div class="text featured col-12">
            <h4>OPCJE DODATKOWE:</h4>

            <ul id="anchor-5">
                <li>fotoksiążka dla Rodziców</li>
                <li>prezentacja multimedialna - pokaz slajdów z podkładem muzycznym,</li>
                <li>możliwość elastycznego dopasowania oferty pakietowej do indywidualnych życzeń Klientów,</li>
                <li>dojazd do 50 km w cenie pakietu (powyżej 0,50zł za 1km wg. Google Maps)</li>
                <li>otrzymanie wszystkich zdjęć które wykona fotograf podczas pracy (bez żadnej obróbki)</li>
            </ul>

            <i>Oprócz fotografowania ślubów chętnie podejmę się przygotowania dla państwa
                reportażu z takich uroczystości jak chrzty, komunie, studniówki i inne imprezy.</i>
        </div>
    </div>
</div>
