<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 17.12.2016
 * Time: 00:27
 */

// CMB2 setup
$entries = get_post_meta( get_the_ID(), '_gallery_repeater', true ); ?>
<div id="gallery" class="module">
    <div class="col-12">
        <h3 class="text-center">Galeria:</h3>
    </div>
	<?php
	$delay = 0;
	$i     = 0;
	foreach ( (array) $entries as $key => $entry ):
		( isset( $entry['_gallery_image'] ) ) ? $galleryImage = esc_html( $entry['_gallery_image'] ) : null;
		( isset( $entry['_gallery_url'] ) ) ? $galleryURL = esc_html( $entry['_gallery_url'] ) : null;
		( isset( $entry['_gallery_bgPosition'] ) ) ? $galleryBgPosition = esc_html( $entry['_gallery_bgPosition'] ) : null;
		echo ( $i % 3 == 0 || $i == 1 ) ? '<div class="row">' : null; ?>

        <a class="col-12 col-sm px-0" href="<?= $galleryURL ?>" data-aos="fade-up" data-aos-anchor="#gallery" data-aos-delay="<?=
        $delay
		?>">
            <div class="single" style="background-image: url(<?= $galleryImage ?>); background-position: <?= $galleryBgPosition ?>">
                <p>Zobacz galerię</p>
            </div>
        </a>
		<?php
		$i ++;
		$delay = $delay + 200;


		echo ( $i % 3 == 0 || $i == 1 ) ? '</div>' : null;


	endforeach; ?>
</div>