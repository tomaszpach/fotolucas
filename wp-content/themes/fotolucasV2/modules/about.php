<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 16.12.2016
 * Time: 23:35
 */ // Grab the metadata from the database
$bgImage = get_post_meta( get_the_ID(), '_about_image', true );
if ($bgImage) {
    $bgImageUrl = 'style="background-image: url(' . $bgImage . ')"';
}
?>

<div id="about" class="module">
    <div class="part image col-12 col-sm-6" <?= $bgImage ? $bgImageUrl : null ?> data-aos="fade-right"></div>

    <div class="part text col-12 col-sm-6" data-aos="fade-left" data-aos-delay="300">
        <h4>O mnie</h4>
        <div class="separator short">
            <span></span>
        </div>
        <div class="description">
            <?= wpautop( get_post_meta( get_the_ID(), '_about_editor', true ) ); ?>
        </div>
    </div>
    <div class="clearfix"></div>
</div>