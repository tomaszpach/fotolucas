<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package FotoLucas
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title( '' ); ?> » <?php
		bloginfo( 'name' ) ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond|Raleway" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header>
    <div class="desktop-nav col-md-12">
		<?php
        if (is_front_page()) {
	        wp_nav_menu( array(
		        'theme_location'  => 'header-homepage-menu',
		        'container_class' => 'menu-wrapper'
	        ) );
        } else {
	        wp_nav_menu( array(
		        'theme_location'  => 'header-second-menu',
		        'container_class' => 'menu-wrapper'
	        ) );
        } ?>
    </div>

    <div class="mobile-nav d-md-none">
        <div class="social">
			<?php include( 'header_parts/social-media.php' ); ?>
        </div>

        <button class="hamburger hamburger--squeeze" type="button">
            <span class="hamburger-box">
            <span class="hamburger-inner"></span>
            </span>
        </button>
    </div>
</header>