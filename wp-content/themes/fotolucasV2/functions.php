<?php

/**
 * Load requred parts
 */

//require('includes/htmlMinify.php');
require( 'includes/gallery-post-type.php' );
require( 'includes/cmb2.php' );

/**
 * Defined constants
 */
define( 'TEMPLATE_DIR_PATH', get_template_directory() );
define( 'TEMPLATE_DIR_URI', get_template_directory_uri() );
define( 'ASSETS', get_template_directory_uri() . '/assets/' );
define( 'HOME_URL', get_home_url() );

/**
 * Enqueue scripts and styles.
 */
function fotolucas_scripts() {
	/**
	 * Load vendor libs.
	 */
	if ( is_file( TEMPLATE_DIR_PATH . '/assets/libs/register_vendors.php' ) ) {
		include_once TEMPLATE_DIR_PATH . '/assets/libs/register_vendors.php';
	}

	/**
	 * Theme styles (CSS) and scripts (JavaScript)
	 */

	wp_localize_script( 'jquery', 'Ajax',
		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	wp_enqueue_style( 'Fonts' );

	// Scripts
	wp_enqueue_script( 'BootstrapJS' );
	wp_enqueue_script( 'TetherJS' );
	wp_enqueue_script( 'masonry' );

	// Styles
	wp_enqueue_style( 'Bootstrap' );
	wp_enqueue_style( 'Tether' );
	wp_enqueue_style( 'Tether-theme' );

	wp_enqueue_style( 'hamburgers', TEMPLATE_DIR_URI . '/assets/styles/hamburgers/hamburgers.css', array(), false );

	// Enqueue Main styles from CSS folder.
	foreach ( glob( TEMPLATE_DIR_PATH . '/assets/styles/scss/partials/*.css', GLOB_NOSORT ) as $file ) {
		$fileName = basename( $file );
		wp_enqueue_style( "{$fileName}", TEMPLATE_DIR_URI . "/assets/styles/scss/partials/{$fileName}", array(), false );
	}

	// Enqueue modules styles from CSS folder.
	foreach ( glob( TEMPLATE_DIR_PATH . '/assets/styles/scss/partials/modules/*.css', GLOB_NOSORT ) as $file ) {
		$fileName = basename( $file );
		wp_enqueue_style( "{$fileName}", TEMPLATE_DIR_URI . "/assets/styles/scss/partials/modules/{$fileName}", array(), false );
	}

	wp_enqueue_script( 'script', ASSETS . 'js/script.js', array(), '2.0', true );

	wp_localize_script( 'script', 'ajaxpagination', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' )
	) );


}

add_action( 'wp_enqueue_scripts', 'fotolucas_scripts' );

/**
 *  Add Post Thumbnails support.
 */
add_theme_support( 'post-thumbnails' );

/**
 * This theme uses wp_nav_menu() in one location.
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'header-homepage-menu' => __( 'Header homepage Menu' ),
			'header-second-menu' => __( 'Header second Menu' ),
			'footer-menu' => __( 'Footer Menu' )
		)
	);
}

add_action( 'init', 'register_my_menus' );