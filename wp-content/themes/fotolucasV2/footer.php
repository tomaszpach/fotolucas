<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package FotoLucas
 */

?>

<footer class="footer">

    <div class="back">
        <div class="angle-up-wrapper">
            <i class="fa fa-angle-up" aria-hidden="true"></i>
        </div>
        <span>Powrót na górę</span>
    </div>
    <div class="copyright">
        @2017 by Łukasz Kogut. Proudly created by WebHouse
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
