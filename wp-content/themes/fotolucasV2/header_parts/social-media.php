<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 05.01.2017
 * Time: 19:46
 */ ?>


<a href="https://www.facebook.com/fotolucasnet" target="_blank">
    <div class="facebook">
        <i class="fa fa-facebook" aria-hidden="true"></i>
    </div>
</a>
<a href="https://vimeo.com/fotolucas" target="_blank">
    <div class="vimeo">
        <i class="fa fa-vimeo" aria-hidden="true"></i>
    </div>
</a>
<a href="https://www.instagram.com/studiofotolucas/" target="_blank">
    <div class="instagram">
        <i class="fa fa-instagram" aria-hidden="true"></i>
    </div>
</a>
<a href="https://www.snapchat.com/add/idudik" target="_blank">
    <div class="snapchat">
        <i class="fa fa-snapchat-ghost" aria-hidden="true"></i>
    </div>
</a>
