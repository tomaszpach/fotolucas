<?php  get_header();  ?>

    <div class="page404">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading">404</h2>
                    <h5 class="subHeading">Strona nieznaleziona</h5>
                    <p class="paragraph">Strona, której szukasz nie istnieje, lub wystapił inny problem.
                        <br><a href="javascript:history.back()" class="back">Wróć</a> lub przejdź do <a href="<?php
                        HOME_URL ?>/" class="homepage">Strony Głównej</a> i wybierz nową stronę.</p>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>