<?php
/**
 * Created by PhpStorm.
 * User: Tomasz
 * Date: 10.01.2016
 * Time: 15:32
 */

get_header(); ?>

    <div class="vertical">
        <div class="social hidden-sm-down navbar-toggleable-sm">
            <?php include('header_parts/social-media.php'); ?>
        </div>
    </div>

<div class="modules">
<?php
/**
 * Include all required modules
 */
require( 'modules/top-banner.php' );
require( 'modules/about.php' );
require( 'modules/gallery.php' );
require( 'modules/quotation.php' );
require( 'modules/offer.php' );
require( 'modules/contact.php' );
require( 'modules/partners.php' ); ?>

</div>


<?php get_footer(); ?>