<?php

/**
 * Libs folder is for all external libraries (js, images etc.)
 *
 * This file contains only wp_register_script() and wp_register_style() for vendor libs.
 * For enqueue this, use functions.php or pagebox module config.
 */

if( defined( 'TEMPLATE_DIR_URI' )) {
    define( 'TEMPLATE_VENDOR_URI', TEMPLATE_DIR_URI . '/assets/libs' );
} else {
    define( 'TEMPLATE_VENDOR_URI', get_template_directory_uri() . '/assets/libs' );
}

/**
 * Unregister WordPress jQuery
 */
//wp_deregister_script( 'jquery' );

/**
 * JS files
 */
//wp_register_script( 'jquery3', TEMPLATE_VENDOR_URI . '/jquery/jquery-3.1.1.min.js', array( ), '3.1.1', true );
//wp_register_script( 'jqueryMigrate', TEMPLATE_VENDOR_URI . '/jquery/jquery-migrate-1.4.1.min.js', array( ), '1.4.1', true );
wp_register_script( 'BootstrapJS', TEMPLATE_VENDOR_URI . '/bootstrap/js/bootstrap.min.js', array( 'jquery3', 'TetherJS' ), 'v4.0.0-alpha.5', true );
wp_register_script( 'TetherJS', TEMPLATE_VENDOR_URI . '/tether/js/tether.min.js', array( 'jquery3' ), '1.3.3', true );
//wp_register_script( 'masonry', TEMPLATE_VENDOR_URI . '/masonry/masonry.min.js', array( 'jquery3' ), '4.1.1', true );
//wp_register_script( 'script', ASSETS . '/js/script.js', array( 'jquery3' ), '4.1.1', true );


/**
 * CSS files
 */
wp_register_style( 'Bootstrap', TEMPLATE_VENDOR_URI . '/bootstrap/css/bootstrap.css', array( 'Fontawesome' ), '3.3.7' );
wp_register_style( 'BootstrapFonts', TEMPLATE_VENDOR_URI . '/bootstrap/fonts/glyphicons-halflings-regular.svg', array( ), '3.3.7' );
wp_register_style( 'Tether', TEMPLATE_VENDOR_URI . '/tether/css/tether.css', array( ), '3.3.7' );
wp_register_style( 'Tether-theme', TEMPLATE_VENDOR_URI . '/tether/css/tether-theme-basic.css', array( ), '3.3.7' );
wp_register_style( 'Fontawesome', TEMPLATE_VENDOR_URI . '/fontawesome/css/font-awesome.min.css', array( ), '4.7.0' );

/**
 * Fonts
 */
wp_register_style('Fonts', ASSETS . 'styles/fonts.css', array(), '1.0.0');