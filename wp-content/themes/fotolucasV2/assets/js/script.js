jQuery(document).ready(function ($) {

    /**
     * Always use '$name' style for variable which contains jQuery element.
     * Cache often used DOM elements in variable.
     */

    var $window = $(window);
    var windowWidth = $window.width();

    /**
     * Check window.width() and enable/disable mobile scripts
     */
    var $header = $('header');
    var $mobileHeaderSelectors = $header.find('.hamburger, .desktop-nav .menu .menu-item-type-custom a');
    var $navLi = $header.find('.desktop-nav .menu li');
    var $hamburger = $header.find('.hamburger');
    var $desktopNav = $header.find('.desktop-nav');

    var windowWidthFunc = debounce(function () {
        var windowWidth = $window.width();

        if (windowWidth <= 768) {
            /**
             * Open menu when click on hamburger
             */
            $mobileHeaderSelectors.on('click', function () {
                $header.toggleClass('mobile-menu-active');
                $desktopNav.toggleClass('mobile');
                $hamburger.toggleClass('is-active');
            });

            $navLi.on('click', function (e) {
                $navLi.removeClass('current-menu-item');
                $(this).addClass('current-menu-item');
            });
        }

        if (windowWidth > 768) {
            $mobileHeaderSelectors.off('click');
        }
    }, 150);


    /**
     * Smooth scroll when click on anchor. Remove current-menu-item class and add to this
     */
    $navLi.first().addClass('current-menu-item');
    $(document).on('click', '.desktop-nav .menu .menu-item-type-custom a', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 63
        }, 500);
    });
    $navLi.on('click', function (e) {
        $navLi.removeClass('current-menu-item');
        $(this).addClass('current-menu-item');
    });

    /**
     * Enable AOS animations
     */
    AOS.init({
        disable: 'mobile',
        once: true
    });


    /**
     * Masonry.js
     */

    var $grid = $('.gallery');
    $grid.masonry({
        percentPosition: true,
        itemSelector: '.gallery-item',
        columnWidth: '.gallery-item',
        transitionDuration: '0.8s'
    });


    var reloadMasonry = debounce(function () {
      $grid.masonry();
      console.log('reload masonry');
    }, 50);

    /**
     * Scroll to top animation
     */
    $('.fa-angle-up').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        });
    });


    /**
     * Debounce function. Really important to use when working with for example resize or scroll to not fire event
     * every time when you scroll or resize.
     * @param func
     * @param wait
     * @param immediate
     * @returns {Function}
     */
    function debounce(func, wait, immediate) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    }



    $window.on('scroll', reloadMasonry);
    $window.on("resize", windowWidthFunc);
    $window.on('load', windowWidthFunc)
});