<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 07.01.2017
 * Time: 16:20
 */

// Register Custom Post Type
function galeria() {

    $labels = array(
        'name'                  => _x( 'Galeria', 'Post Type General Name', 'galeria' ),
        'singular_name'         => _x( 'Galeria', 'Post Type Singular Name', 'galeria' ),
        'menu_name'             => __( 'Galeria', 'galeria' ),
        'name_admin_bar'        => __( 'Galeria', 'galeria' ),
        'archives'              => __( 'Archiwum galerii', 'galeria' ),
        'attributes'            => __( 'Atrybuty galerii', 'galeria' ),
        'parent_item_colon'     => __( 'Rodzic galerii:', 'galeria' ),
        'all_items'             => __( 'Wszystkie galerie', 'galeria' ),
        'add_new_item'          => __( 'Dodaj nową galerie', 'galeria' ),
        'add_new'               => __( 'Dodaj galerię', 'galeria' ),
        'new_item'              => __( 'Nowa galeria', 'galeria' ),
        'edit_item'             => __( 'Edytuj galerię', 'galeria' ),
        'update_item'           => __( 'Aktualizuj Galeria', 'galeria' ),
        'view_item'             => __( 'Zobacz Galeria', 'galeria' ),
        'view_items'            => __( 'Zobacz Galeria', 'galeria' ),
        'search_items'          => __( 'Szukaj Galeria', 'galeria' ),
        'not_found'             => __( 'Not found', 'galeria' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'galeria' ),
        'featured_image'        => __( 'Featured Image', 'galeria' ),
        'set_featured_image'    => __( 'Set featured image', 'galeria' ),
        'remove_featured_image' => __( 'Remove featured image', 'galeria' ),
        'use_featured_image'    => __( 'Use as featured image', 'galeria' ),
        'insert_into_item'      => __( 'Insert into Galeria', 'galeria' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'galeria' ),
        'items_list'            => __( 'Items list', 'galeria' ),
        'items_list_navigation' => __( 'Items list navigation', 'galeria' ),
        'filter_items_list'     => __( 'Filter items list', 'galeria' ),
    );
    $args = array(
        'label'                 => __( 'Galeria', 'galeria' ),
        'description'           => __( 'Niesamowita galeria', 'galeria' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', ),
        'taxonomies'            => array( 'galeria' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'galeria', $args );

}
add_action( 'init', 'galeria', 0 );