<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 07.01.2017
 * Time: 16:56
 */

add_action( 'cmb2_admin_init', 'gallery_metabox' );
/**
 * Define the metabox and field configurations.
 */
function gallery_metabox() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_gallery_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'gallery',
        'title'         => __( 'Gallery module', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $group_field_id = $cmb->add_field( array(
        'id'          => $prefix . 'repeater',
        'type'        => 'group',
        'description' => __( 'Generates reusable form entries', 'cmb2' ),
        // 'repeatable'  => false, // use false if you want non-repeatable group
        'options'     => array(
            'group_title'   => __( 'Entry {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
            'add_button'    => __( 'Add Another Entry', 'cmb2' ),
            'remove_button' => __( 'Remove Entry', 'cmb2' ),
            'sortable'      => true, // beta
            // 'closed'     => true, // true to have the groups closed by default
        ),
    ) );

// Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb->add_group_field( $group_field_id, array(
        'name'    => 'Top banner image',
        'desc'    => 'Select or upload image for your top banner module',
        'id'      => $prefix . 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select image' // Change upload button text. Default: "Add or Upload File"
        ),
         'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );

    $cmb->add_group_field( $group_field_id, array(
        'name'    => 'URL',
        'desc'    => 'Please type URL to gallery page',
        'default' => 'URL to gallery page',
        'id'      => $prefix . 'url',
        'type'    => 'text',
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name'    => 'bgPosition',
		'desc'    => 'Set background image position',
		'default' => '50% 50%',
		'id'      => $prefix . 'bgPosition',
		'type'    => 'text',
	) );
}