<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 07.01.2017
 * Time: 16:56
 */

add_action( 'cmb2_admin_init', 'quote_metabox' );
/**
 * Define the metabox and field configurations.
 */
function quote_metabox() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_quote_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'quote',
        'title'         => __( 'Quote module', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $cmb->add_field( array(
        'name'    => 'Quote',
        'desc'    => 'Paste quote',
        'id'      => $prefix . 'editor',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );

    $cmb->add_field( array(
        'name'    => 'Quote image',
        'desc'    => 'Select image to be in background',
        'id'      => $prefix . 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
            'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
        ),
    ) );
}