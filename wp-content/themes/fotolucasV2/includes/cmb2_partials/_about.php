<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 07.01.2017
 * Time: 16:56
 */

add_action( 'cmb2_admin_init', 'about_metabox' );
/**
 * Define the metabox and field configurations.
 */
function about_metabox() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_about_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'about',
        'title'         => __( 'About me module', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $cmb->add_field( array(
        'name'    => 'About me',
        'desc'    => 'Describe me',
        'id'      => $prefix . 'editor',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );

    $cmb->add_field( array(
        'name'    => 'About me',
        'desc'    => 'Select image',
        'id'      => $prefix . 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
            'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
        ),
    ) );
}