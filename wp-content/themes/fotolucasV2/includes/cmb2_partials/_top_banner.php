<?php
/**
 * Created by PhpStorm.
 * User: nurtureagency
 * Date: 07.01.2017
 * Time: 16:52
 */

add_action( 'cmb2_admin_init', 'top_banner_metabox' );
/**
 * Define the metabox and field configurations.
 */
function top_banner_metabox() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_top_banner_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'top_banner',
        'title'         => __( 'Top banner module', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $cmb->add_field( array(
        'name'    => 'Top banner image',
        'desc'    => 'Select or upload image for your top banner module',
        'id'      => $prefix . 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
            'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
            'add_upload_file_text' => 'Select image' // Change upload button text. Default: "Add or Upload File"
        ),
    ) );
}