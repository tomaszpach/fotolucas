<?php get_header(); ?>

<div class="featured-image container-fluid" style="background-image: url(<?= the_post_thumbnail_url(); ?>)">
    <h3 class="title"><?= get_the_title(); ?></h3>
</div>


<?php if (have_posts()) :
    while (have_posts()) :
        the_post();
        the_content();
    endwhile;
endif;

get_footer();